const { Builder, By, Key, until, Capability, Capabilities, Button } = require('selenium-webdriver');
const fs = require('fs');
const path = require('path');
// const sel = require('selenium-webdriver');
let chrome = require('selenium-webdriver/chrome');
let chromedriver = require('chromedriver');
const email = require('./email').email;

(async function test() {
    console.log('here');
    const imgDir = './docImg/';

    fs.readdir(imgDir, (err, files) => {
        if (err) throw err;

        for (const file of files) {
            fs.unlink(path.join(imgDir, file), err => {
                if (err) throw err;
            });
        }
    });
    let driver = await new Builder().forBrowser('chrome').build();
    // console.log(driver);
    try {
        //Define variables
        var pngCounter = 1;
        var username = 'maya@atlas.org.il';
        var pw = '7597818';
        var url = 'https://www.foundationatlas.com/he/';

        // Navigate to Url
        await driver.get(url);


        function snapshot() {
            driver.takeScreenshot().then(
                function (image, err) {
                    if (err) console.log(err);
                    var fileName = imgDir + pngCounter + '' + '.png';
                    fs.writeFile(fileName, image, 'base64', function (err) {
                        pngCounter++
                        if (err) console.log(err);
                    });
                }
            )
                .catch(err => {
                    console.error(err);
                })

        }
        // var d = await driver.findElement(By.id('fn'))
        // console.log(d);
        snapshot();
        await driver.findElement(By.name('username')).sendKeys(username);

        await driver.findElement(By.name('password')).sendKeys(pw, Key.ENTER);


        await driver.getCurrentUrl().then(newUrl => {
            if (newUrl !== url) {
                snapshot();
                console.error('Wrong username or password');
                driver.quit();
            } else {
                snapshot();
            }
        })

        // await driver.findElement(By.tagName("tasks")).isDisplayed().then(res => {
        //     console.log('Tasks exists');
        //     console.log(res);
        // })
        // .catch(err => {
        //     console.error('No Tasks!');
        // })
        /***PICKING SEARCH CRTITERIAS OF RFPS***/
        await driver.findElement(By.id('tab_op')).click();
        
        await driver.findElement(By.id('type_txt')).click(); // types dropdown
        await driver.findElement(By.xpath(`//*[@id="type_lbls"]/label[1]/i[1]`)).click(); // pick type
        await driver.findElement(By.xpath(`//*[@id="type_drop"]/div[1]/a/i`)).click(); // confirm types dropdown
        snapshot();
        await driver.findElement(By.id('opsb_txt')).click(); // subjects dropdown
        await driver.findElement(By.xpath(`//*[@id="opsb_lbls"]/label[1]/i[1]`)).click(); // pick subject
        await driver.findElement(By.xpath(`//*[@id="opsb_drop"]/div[1]/a/i`)).click(); // pick country
        snapshot();
        
        await driver.findElement(By.id('type_txt')).click(); // type dropdown
        await driver.findElement(By.xpath(`//*[@id="type_lbls"]/label[1]/i[1]`)).click(); // pick country
        await driver.findElement(By.xpath(`//*[@id="type_lbls"]/label[1]/i[1]`)).click(); // pick country
        snapshot();
        await driver.findElement(By.id('type_txt')).click(); // type dropdown
        await driver.findElement(By.xpath(`//*[@id="type_lbls"]/label[1]/i[1]`)).click(); // pick country
        await driver.findElement(By.xpath(`//*[@id="type_lbls"]/label[1]/i[1]`)).click(); // pick country
        snapshot();

        await driver.findElement(By.xpath('//*[@id="country_drop"]/div[1]/a[2]/i')).click(); // confirm countries dropdown
        //*[@id="country_lbls"]/label[1]/i[1]

        await driver.findElement(By.xpath(`//*[@id="type_lbls"]/label[1]/i[1]`)).click(); // pick country
        await driver.findElement(By.xpath('//*[@id="country_drop"]/div[1]/a[2]/i')).click(); // confirm countries dropdown

        await driver.findElement(By.id('country_txt')).click(); // country dropdown
        //*[@id="country_lbls"]/label[1]/i[1]

        await driver.findElement(By.xpath(`//*[@id="country_lbls"]/label[1]/i[1]`)).click(); // pick country
        await driver.findElement(By.xpath('//*[@id="country_drop"]/div[1]/a[2]/i')).click(); // confirm countries dropdown




        /***PICKING SEARCH CRTITERIAS OF COMPANIES***/
        await driver.findElement(By.id('tab_bz')).click(); // go to companies form tab
        
        await driver.findElement(By.id('subj_txt')).click();
        
        var subjLength = await driver.findElement(By.id('subj_lbls')) // find how many fields to choose from exist
        .findElements(By.tagName('label'))
                .then(function (e) {
                // console.log(e.length);
                return e.length;
            });
            console.log(subjLength);
            for (var i = 1; i <= subjLength; i++) {
                await driver.findElement(By.xpath(`//*[@id="subj_lbls"]/label[${i}]/i[1]`)).click(); //click on each field exists in view of subjects
            }
            snapshot();
            //*[@id="subj_drop"]/div[1]/a[2]/i
            await driver.findElement(By.xpath('//*[@id="subj_drop"]/div[1]/a[2]/i')).click(); // confirm subjects dropdown

            //Country:
            await driver.findElement(By.id('country_txt')).click(); // country dropdown
            //*[@id="country_lbls"]/label[1]/i[1]

            await driver.findElement(By.xpath(`//*[@id="country_lbls"]/label[1]/i[1]`)).click(); // pick country
            await driver.findElement(By.xpath('//*[@id="country_drop"]/div[1]/a[2]/i')).click(); // confirm countries dropdown
            snapshot();

            await driver.findElement(By.xpath(`//*[@id="srchan"]/a/i`)).click(); // Press search button
        


        /***PICKING SEARCH CRTITERIAS OF FUNDS***/
        await driver.findElement(By.id('tab_fd')).click(); // go to funds form tab

        await driver.findElement(By.xpath(`//*[@id="srchan"]/a/i`)).click(); // Press search button (with same search criterias of companies)

        //Subjects:
            //*[@id="fd_holder"]/div[2]/h4

        // await (await driver.findElement(By.id('subj_lbls'))).isSelected().then(res => {
            //     console.log(res);
            // });
            snapshot();
            // driver.getPageSource().then(r => {
        //     console.log(r);
        // })
        // driver.getTitle().then(r => {
        //     console.log(r);
        // })


        // await driver.findElement(By.

        // let firstResult = await driver.wait(until.elementLocated(By.css('h3>div')), 10000);

        // console.log(await firstResult.getAttribute('textContent'));
    }
    finally {
        // console.log(driver.getPageSource());
        driver.quit();
        // email();
    }
})();