exports.email = () => {
const sendmail = require('sendmail')({silent: true})
const fs = require('fs');

var files = fs.readdirSync('docImg');
var attach_arr = []
files.forEach(file => {
    var obj = {
        path: `./docImg/${file}`
    }
    attach_arr.push(obj);
})
console.log(attach_arr);

sendmail({
from: 'donotreply@atlas.org.il',
  to: 'mayag101010@gmail.com',
//   replyTo: 'jason@yourdomain.com',
  subject: 'תוצאות בדיקת מערכת',
  html: 'Screenshots from tests: ',
  attachments: attach_arr
}, function (err, reply) {
  console.log(err && err.stack)
  console.dir(reply)
})
}